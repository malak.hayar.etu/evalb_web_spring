# Malak Hayar - Groupe M

# BDD utilisée : Postgresql

# Creation automatique table : OUI

# Lien gitlab : https://gitlab.univ-lille.fr/malak.hayar.etu/evalb_web_spring

# Questions : 

## Q1 : fait
## Q2 : fait
## Q3 : fait
## Q4 : fait
## Q5/Q6/Q7/Q8 : pas fait


# Version de la seconde chance : pourquoi ça n'a pas marché ?

* la conf de postgres dans application.properties était érronée
* J'ai oublié le Autowired 
* je voulais faire du thymeleaf pour pas faire les jsp mais ça me posait trop de problème du coup j'ai switch pour les jsp
* import.sql n'était pas dans le bon repertoire et il ne crée pas les tables car j'avais fais une faute d'orthographe sur "active(r)"... (envie de pleurer)
* Maintenant tout ce qui est spring security marche !!

# Questions (version 2)

## Q1 : fait
## Q2 : fait
## Q3 : fait
## Q4 : fait
## Q5 : fait
## Q6 : fait
## Q7 : fait
## Q8 : fait

***Merci beaucoup encore de m'avoir permit de refaire le CTP***