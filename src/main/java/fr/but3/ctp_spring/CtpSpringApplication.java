package fr.but3.ctp_spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CtpSpringApplication {

	public static void main(String[] args) {

		SpringApplication.run(CtpSpringApplication.class, args);
	}

}
