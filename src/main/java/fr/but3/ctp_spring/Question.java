package fr.but3.ctp_spring;

import jakarta.persistence.*;

import java.util.List;

@Entity
@Table(name = "question")
public class Question {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int qno;

    @Column(name = "libquest")
    private String libquest;

    @Column(name = "active")
    private boolean active;


    // Getters and Setters

    public int getQno() {
        return qno;
    }

    public void setQno(int qno) {
        this.qno = qno;
    }

    public String getLibquest() {
        return libquest;
    }

    public void setLibquest(String libquest) {
        this.libquest = libquest;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }



}