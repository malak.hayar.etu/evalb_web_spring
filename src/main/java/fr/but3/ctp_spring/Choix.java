package fr.but3.ctp_spring;


import jakarta.persistence.*;

@Entity
@Table(name = "choix")
public class Choix {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int cno;


    @Column(name = "libchoix")
    private String libchoix;

    @Column(name = "statut")
    private boolean statut;

    @Column(name = "nbchoix")
    private int nbChoix;

    @ManyToOne()
    @JoinColumn(name = "qno")
    private Question question;

    // getters and setters

    public int getCno() {
        return cno;
    }

    public void setCno(int cno) {
        this.cno = cno;
    }

    public String getLibchoix() {
        return libchoix;
    }

    public void setLibchoix(String libchoix) {
        this.libchoix = libchoix;
    }

    public boolean isStatut() {
        return statut;
    }

    public void setStatut(boolean statut) {
        this.statut = statut;
    }

    public int getNbChoix() {
        return nbChoix;
    }

    public void setNbChoix(int nbChoix) {
        this.nbChoix = nbChoix;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }




}
