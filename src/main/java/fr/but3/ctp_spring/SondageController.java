package fr.but3.ctp_spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;
import java.util.List;
import java.util.Optional;

@Controller

public class SondageController {
    @Autowired
    private QuestionRepository questionRepository;
    @Autowired
    private ChoixRepository choixRepository;

    @GetMapping("/activer")
    public String activer(Model model, Principal principal) {

        List<Question> questions = questionRepository.findAll();
        model.addAttribute("questions", questions);
        String name = principal.getName();
        model.addAttribute("username", name);
        return "activer";
    }

    @PostMapping("/activer")
    public String activer(@ModelAttribute(name = "questionId") Integer qno, Model model, Principal principal) {
        // desac toutes les autres questions
        questionRepository.findAll().forEach(question -> {
            question.setActive(false);
            questionRepository.save(question);
        });

        // activer la question selectionnée
        Question selectedQuestion = questionRepository.findById(qno).orElseThrow();
        selectedQuestion.setActive(true);
        questionRepository.save(selectedQuestion);

        // mettre tous les nbchoix à zero
        choixRepository.findByQuestion(selectedQuestion).forEach(choix -> {
            choix.setNbChoix(0);
            choixRepository.save(choix);
        });

        model.addAttribute("message", "La question " +
                selectedQuestion.getLibquest() + " vient d'etr e activée");
        String name = principal.getName();
        model.addAttribute("username", name);
        return "message";


    }

    @GetMapping("/voter")
    public String voter(Model model, Principal principal) {
        // Trouver la question active
        Optional<Question> activeQuestion = questionRepository.findByActiveTrue();

        if (activeQuestion.isPresent()) {
            model.addAttribute("question", activeQuestion.get());
            List<Choix> choixList = choixRepository.findByQuestion(activeQuestion.get());
            model.addAttribute("choixList", choixList);
        } else {
            model.addAttribute("error", "Aucune question n'est activée");
        }

        String name = principal.getName();
        model.addAttribute("username", name);
        return "voter";
    }

    @PostMapping("/voter")
    public String voter(@RequestParam Integer choixId, Model model, Principal principal) {
        // Trouver le choix sélectionné
        Choix selectedChoix = choixRepository.findById(choixId).orElseThrow();

        // Incrémenter le compteur de ce choix
        selectedChoix.setNbChoix(selectedChoix.getNbChoix() + 1);
        choixRepository.save(selectedChoix);
        model.addAttribute("message", "Votre choix a bien été pris en compte");
        String name = principal.getName();
        model.addAttribute("username", name);
        return "message";
    }





    @GetMapping("/voir")
    public String voir(Model model, Principal principal) {
        // Trouver la question active
        Question activeQuestion = questionRepository.findByActiveTrue().orElseThrow();

        // Trouver les choix pour la question active
        List<Choix> choixList = choixRepository.findByQuestion(activeQuestion);

        // Calculer le pourcentage de votes corrects
        long totalVotes = choixList.stream().mapToLong(Choix::getNbChoix).sum();
        long correctVotes = choixList.stream().filter(Choix::isStatut).mapToLong(Choix::getNbChoix).sum();
        double pourcentageCorrect = (totalVotes > 0) ? ((double) correctVotes / totalVotes) * 100 : 0;

        model.addAttribute("question", activeQuestion);
        model.addAttribute("choixList", choixList);
        model.addAttribute("percent", pourcentageCorrect);
        String name = principal.getName();
        model.addAttribute("username", name);
        return "voir";
    }

}
