insert into question(libquest, active) values('Date de la bataille de Marignan ?', FALSE),('Quel est le président de la république actuel ?', TRUE),('Qui a découvert la péniciline ?', FALSE);

insert into choix(qno,libchoix,statut,nbchoix) values(1,'1313',false, 0),(1,'1414',false, 0),(1,'1515',true, 0),(1,'1616',false, 0);
insert into choix(qno,libchoix,statut, nbchoix) values(2,'Chirac',false, 0),(2,'Sarkozy',false, 0),(2,'Hollande',false, 0),(2,'Macron',true, 0);
insert into choix(qno,libchoix,statut, nbchoix) values(3,'Darwin',false, 0),(3,'Pasteur',false, 0),(3,'Fleming',true, 0),(3,'Watson',false, 0);


-- select * from question natural join choix;
-- select libquest, count(*) from question natural join choix group by libquest;
-- select * from question natural join choix where statut=true;
